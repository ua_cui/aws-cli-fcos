# AWS CLI on CoreOS
## Instructions

### Run

```bash

git clone https://bitbucket.org/ua_cui/aws-cli-fcos.git

cd aws-cli-fcos

docker build -t aws-cli .
```

### Configuration files

Make sure `~/.aws/config` and `~/.aws/credentials` exist in your CoreOS instance.

#### `~/.aws/config`

```
[default]
region = us-east-1
```

#### `~/.aws/credentials`

```
[default]
aws_access_key_id = AKIXXXXXXXXXXXXXXXXX
aws_secret_access_key = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

### Run your commands within the container

```bash
docker run -it -v $HOME/.aws:/home/aws/.aws aws-cli <command> <args>
```

#### Examples

**List Buckets**

```bash
docker run -it -v $HOME/.aws:/home/aws/.aws aws-cli aws s3 ls
```

**Get ECR Login**

```bash
docker run -it -v $HOME/.aws:/home/aws/.aws aws-cli aws ecr get-login
```

**Use a named profile**

```bash
run -it -v $HOME/.aws:/home/aws/.aws aws-cli aws ecr get-login --profile groove
```

## See Also

  * https://www.linkedin.com/pulse/use-aws-cli-from-coreos-via-docker-easy-way-john-drago
